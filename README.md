# Debye BEC

Debye-specific plugins and configs for BEC

## How to

### Visual studio code
To open
```
ssh x01da-bec-001
cd /data/test/x01da-test-bec/bec_deployment
code
```
To run tests directly in vs code terminal
```
. /data/test/x01da-test-bec/bec_deployment/bec_venv/bin/activate
cd /data/test/x01da-test-bec/bec_deployment/debye_bec
pytest -vv ./tests
```
### Git
```
git pull
git push origin feat/add_advanced_scan_modes
git status
```
If git claims to not know the author identity
```
git config --global user.email "you@example.com"
git config --global user.name "gac-x01da"
```

### BEC Server
```
ssh x01da-bec-001
cd /data/test/x01da-test-bec/bec_deployment
. /data/test/x01da-test-bec/bec_deployment/bec_venv/bin/activate

bec-server start
bec-server restart
bec-server stop
bec-server attach
```

To restart individual server modules:
- ctrl-c + ctrl-c to stop for example scan server or device server module
- restart server module(s)

### BEC Client
```
ssh x01da-bec-001
cd /data/test/x01da-test-bec/bec_deployment
bec
```

#### Useful commands in bec
Update Session with specific config:
```
bec.config.update_session_with_file("debye_bec/debye_bec/device_configs/x01da_test_config.yaml")
```

Define folder and sample name for written files:
```
bec.system_config.file_directory="test"
bec.system_config.file_suffix ="sampleA"
```