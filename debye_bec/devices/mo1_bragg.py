""" Module for the Mo1 Bragg positioner of the Debye beamline.
The softIOC is reachable via the EPICS prefix X01DA-OP-MO1:BRAGG: and connected
to a motor controller via web sockets. The Mo1 Bragg positioner is not only a 
positioner, but also a scan controller to setup XAS and XRD scans. A few scan modes
are programmed in the controller, e.g. simple and advanced XAS scans + XRD triggering mode.

Note: For some of the Epics PVs, in particular action buttons, the put_complete=True is
used to ensure that the action is executed completely. This is believed
to allow for a more stable execution of the action."""

import enum
import threading
import time
import traceback
from dataclasses import dataclass
from typing import Literal

from bec_lib.logger import bec_logger
from ophyd import Component as Cpt
from ophyd import (
    Device,
    DeviceStatus,
    EpicsSignal,
    EpicsSignalRO,
    EpicsSignalWithRBV,
    Kind,
    PositionerBase,
    Signal,
    Staged,
)
from typeguard import typechecked
from ophyd.utils import LimitError
from ophyd_devices.utils import bec_scaninfo_mixin, bec_utils
from ophyd_devices.utils.errors import DeviceStopError, DeviceTimeoutError
from debye_bec.devices.utils.mo1_bragg_utils import compute_spline

logger = bec_logger.logger

class ScanControlScanStatus(int, enum.Enum):
    """Enum class for the scan status of the Bragg positioner"""

    PARAMETER_WRONG = 0
    VALIDATION_PENDING = 1
    READY = 2
    RUNNING = 3


class ScanControlLoadMessage(int, enum.Enum):
    """Enum for validating messages for load message of the Bragg positioner"""

    PENDING = 0
    STARTED = 1
    SUCCESS = 2
    ERR_XRD_MEAS_LEN_LOW = 3
    ERR_XRD_N_TRIGGERS_LOW = 4
    ERR_XRD_TRIGS_EVERY_N_LOW = 5
    ERR_XRD_MEAS_LEN_HI = 6
    ERR_XRD_N_TRIGGERS_HI = 7
    ERR_XRD_TRIGS_EVERY_N_HI = 8
    ERR_SCAN_HI_ANGLE_LIMIT = 9
    ERR_SCAN_LOW_ANGLE_LIMITS = 10
    ERR_SCAN_TIME = 11
    ERR_SCAN_VEL_TOO_HI = 12
    ERR_SCAN_ANGLE_OUT_OF_LIM = 13
    ERR_SCAN_HIGH_VEL_LAR_42 = 14
    ERR_SCAN_MODE_INVALID = 15


class Mo1BraggError(Exception):
    """Mo1Bragg specific exception"""


class MoveType(str, enum.Enum):
    """Enum class to switch between move types energy and angle for the Bragg positioner"""

    ENERGY = "energy"
    ANGLE = "angle"


class ScanControlMode(int, enum.Enum):
    """Enum class for the scan control mode of the Bragg positioner"""

    SIMPLE = 0
    ADVANCED = 1


class MoveTypeSignal(Signal):
    """Custom Signal to set the move type of the Bragg positioner"""

    # pylint: disable=arguments-differ
    def set(self, value: str | MoveType) -> None:
        """Returns currently active move method

        Args:
            value (str | MoveType) : Can be either 'energy' or 'angle'
        """

        value = MoveType(value.lower())
        self._readback = value.value


class Mo1BraggStatus(Device):
    """Mo1 Bragg PVs for status monitoring"""

    error_status = Cpt(EpicsSignalRO, suffix="error_status_RBV", kind="config", auto_monitor=True)
    brake_enabled = Cpt(EpicsSignalRO, suffix="brake_enabled_RBV", kind="config", auto_monitor=True)
    mot_commutated = Cpt(
        EpicsSignalRO, suffix="mot_commutated_RBV", kind="config", auto_monitor=True
    )
    axis_enabled = Cpt(EpicsSignalRO, suffix="axis_enabled_RBV", kind="config", auto_monitor=True)
    enc_initialized = Cpt(
        EpicsSignalRO, suffix="enc_initialized_RBV", kind="config", auto_monitor=True
    )
    heartbeat = Cpt(EpicsSignalRO, suffix="heartbeat_RBV", kind="config", auto_monitor=True)


class Mo1BraggEncoder(Device):
    """Mo1 Bragg PVs to communicate with the encoder"""

    enc_reinit = Cpt(EpicsSignal, suffix="enc_reinit", kind="config")
    enc_reinit_done = Cpt(EpicsSignalRO, suffix="enc_reinit_done_RBV", kind="config")


class Mo1BraggCrystal(Device):
    """Mo1 Bragg PVs to set the crystal parameters"""

    offset_si111 = Cpt(EpicsSignalWithRBV, suffix="offset_si111", kind="config")
    offset_si311 = Cpt(EpicsSignalWithRBV, suffix="offset_si311", kind="config")
    xtal_enum = Cpt(EpicsSignalWithRBV, suffix="xtal_ENUM", kind="config")
    d_spacing_si111 = Cpt(EpicsSignalWithRBV, suffix="d_spacing_si111", kind="config")
    d_spacing_si311 = Cpt(EpicsSignalWithRBV, suffix="d_spacing_si311", kind="config")
    set_offset = Cpt(EpicsSignal, suffix="set_offset", kind="config", put_complete=True)
    current_xtal = Cpt(
        EpicsSignalRO, suffix="current_xtal_ENUM_RBV", kind="normal", auto_monitor=True
    )


class Mo1BraggScanSettings(Device):
    """Mo1 Bragg PVs to set the scan setttings"""

    # XRD settings
    xrd_select_ref_enum = Cpt(EpicsSignalWithRBV, suffix="xrd_select_ref_ENUM", kind="config")
    xrd_enable_hi_enum = Cpt(EpicsSignalWithRBV, suffix="xrd_enable_hi_ENUM", kind="config")

    xrd_time_hi = Cpt(EpicsSignalWithRBV, suffix="xrd_time_hi", kind="config")
    xrd_n_trigger_hi = Cpt(EpicsSignalWithRBV, suffix="xrd_n_trigger_hi", kind="config")
    xrd_every_n_hi = Cpt(EpicsSignalWithRBV, suffix="xrd_every_n_hi", kind="config")

    xrd_enable_lo_enum = Cpt(EpicsSignalWithRBV, suffix="xrd_enable_lo_ENUM", kind="config")
    xrd_time_lo = Cpt(EpicsSignalWithRBV, suffix="xrd_time_lo", kind="config")
    xrd_n_trigger_lo = Cpt(EpicsSignalWithRBV, suffix="xrd_n_trigger_lo", kind="config")
    xrd_every_n_lo = Cpt(EpicsSignalWithRBV, suffix="xrd_every_n_lo", kind="config")

    # XAS simple scan settings
    s_scan_angle_hi = Cpt(EpicsSignalWithRBV, suffix="s_scan_angle_hi", kind="config")
    s_scan_angle_lo = Cpt(EpicsSignalWithRBV, suffix="s_scan_angle_lo", kind="config")
    s_scan_energy_lo = Cpt(
        EpicsSignalWithRBV, suffix="s_scan_energy_lo", kind="config", auto_monitor=True
    )
    s_scan_energy_hi = Cpt(
        EpicsSignalWithRBV, suffix="s_scan_energy_hi", kind="config", auto_monitor=True
    )
    s_scan_scantime = Cpt(
        EpicsSignalWithRBV, suffix="s_scan_scantime", kind="config", auto_monitor=True
    )

    # XAS advanced scan settings
    a_scan_pos = Cpt(EpicsSignalWithRBV, suffix="a_scan_pos", kind="config", auto_monitor=True)
    a_scan_vel = Cpt(EpicsSignalWithRBV, suffix="a_scan_vel", kind="config", auto_monitor=True)
    a_scan_time = Cpt(EpicsSignalWithRBV, suffix="a_scan_time", kind="config", auto_monitor=True)

class Mo1BraggCalculator(Device):
    """Mo1 Bragg PVs to convert angle to energy or vice-versa."""

    calc_reset = Cpt(EpicsSignal, suffix="calc_reset", kind="config", put_complete=True)
    calc_done = Cpt(EpicsSignalRO, suffix="calc_done_RBV", kind="config")
    calc_energy = Cpt(EpicsSignalWithRBV, suffix="calc_energy", kind="config")
    calc_angle = Cpt(EpicsSignalWithRBV, suffix="calc_angle", kind="config")

class Mo1BraggScanControl(Device):
    """Mo1 Bragg PVs to control the scan after setting the parameters."""

    scan_mode_enum = Cpt(EpicsSignalWithRBV, suffix="scan_mode_ENUM", kind="config")
    scan_duration = Cpt(
        EpicsSignalWithRBV, suffix="scan_duration", kind="config", auto_monitor=True
    )
    scan_load = Cpt(EpicsSignal, suffix="scan_load", kind="config", put_complete=True)
    scan_msg = Cpt(EpicsSignalRO, suffix="scan_msg_ENUM_RBV", kind="config", auto_monitor=True)
    scan_start_infinite = Cpt(
        EpicsSignal, suffix="scan_start_infinite", kind="config", put_complete=True
    )
    scan_start_timer = Cpt(
        EpicsSignal, suffix="scan_start_timer", kind="config", put_complete=True
    )
    scan_stop = Cpt(EpicsSignal, suffix="scan_stop", kind="config", put_complete=True)
    scan_status = Cpt(
        EpicsSignalRO, suffix="scan_status_ENUM_RBV", kind="config", auto_monitor=True
    )
    scan_time_left = Cpt(
        EpicsSignalRO, suffix="scan_time_left_RBV", kind="config", auto_monitor=True
    )
    scan_done = Cpt(EpicsSignalRO, suffix="scan_done_RBV", kind="config", auto_monitor=True)
    scan_val_reset = Cpt(
        EpicsSignal, suffix="scan_val_reset", kind="config", put_complete=True
    )
    scan_progress = Cpt(EpicsSignalRO, suffix="scan_progress_RBV", kind="config", auto_monitor=True)
    scan_spectra_done = Cpt(
        EpicsSignalRO, suffix="scan_n_osc_RBV", kind="config", auto_monitor=True
    )
    scan_spectra_left = Cpt(
        EpicsSignalRO, suffix="scan_n_osc_left_RBV", kind="config", auto_monitor=True
    )


@dataclass
class ScanParameter:
    """Dataclass to store the scan parameters for the Mo1 Bragg positioner.
    This needs to be in sync with the kwargs of the MO1 Bragg scans from Debye, to
    ensure that the scan parameters are correctly set. Any changes in the scan kwargs,
    i.e. renaming or adding new parameters, need to be represented here as well."""

    scan_time: float = None
    scan_duration: float = None
    xrd_enable_low: bool = None
    xrd_enable_high: bool = None
    num_trigger_low: int = None
    num_trigger_high: int = None
    exp_time_low: float = None
    exp_time_high: float = None
    cycle_low: int = None
    cycle_high: int = None
    start: float = None
    stop: float = None
    p_kink: float = None
    e_kink: float = None


class Mo1Bragg(Device, PositionerBase):
    """Class for the Mo1 Bragg positioner of the Debye beamline.
    The prefix to connect to the soft IOC is X01DA-OP-MO1:BRAGG: which is connected to
    the NI motor controller via web sockets.
    """

    USER_ACCESS = []

    crystal = Cpt(Mo1BraggCrystal, "")
    encoder = Cpt(Mo1BraggEncoder, "")
    scan_settings = Cpt(Mo1BraggScanSettings, "")
    calculator = Cpt(Mo1BraggCalculator, "")
    scan_control = Cpt(Mo1BraggScanControl, "")
    status = Cpt(Mo1BraggStatus, "")

    # signal to indicate the move type 'energy' or 'angle'
    move_type = Cpt(MoveTypeSignal, value=MoveType.ENERGY, kind="config")

    # Energy PVs
    readback = Cpt(
        EpicsSignalRO, suffix="feedback_pos_energy_RBV", kind="hinted", auto_monitor=True
    )
    setpoint = Cpt(
        EpicsSignalWithRBV, suffix="set_abs_pos_energy", kind="normal", auto_monitor=True
    )
    motor_is_moving = Cpt(
        EpicsSignalRO, suffix="move_abs_done_RBV", kind="normal", auto_monitor=True
    )
    low_lim = Cpt(EpicsSignalRO, suffix="lo_lim_pos_energy_RBV", kind="config", auto_monitor=True)
    high_lim = Cpt(EpicsSignalRO, suffix="hi_lim_pos_energy_RBV", kind="config", auto_monitor=True)
    velocity = Cpt(EpicsSignalWithRBV, suffix="move_velocity", kind="config", auto_monitor=True)

    # Angle PVs
    # TODO make angle motion a pseudo motor
    feedback_pos_angle = Cpt(
        EpicsSignalRO, suffix="feedback_pos_angle_RBV", kind="normal", auto_monitor=True
    )
    setpoint_abs_angle = Cpt(
        EpicsSignalWithRBV, suffix="set_abs_pos_angle", kind="normal", auto_monitor=True
    )
    low_limit_angle = Cpt(
        EpicsSignalRO, suffix="lo_lim_pos_angle_RBV", kind="config", auto_monitor=True
    )
    high_limit_angle = Cpt(
        EpicsSignalRO, suffix="hi_lim_pos_angle_RBV", kind="config", auto_monitor=True
    )

    # Execute motion
    move_abs = Cpt(EpicsSignal, suffix="move_abs", kind="config", put_complete=True)
    move_stop = Cpt(EpicsSignal, suffix="move_stop", kind="config", put_complete=True)

    SUB_READBACK = "readback"
    _default_sub = SUB_READBACK
    SUB_PROGRESS = "progress"

    def __init__(
        self, prefix="", *, name: str, kind: Kind = None, device_manager=None, parent=None, **kwargs
    ):
        """Initialize the Mo1 Bragg positioner.

        Args:
            prefix (str): EPICS prefix for the device
            name (str): Name of the device
            kind (Kind): Kind of the device
            device_manager (DeviceManager): Device manager instance
            parent (Device): Parent device
            kwargs: Additional keyword arguments
        """
        super().__init__(prefix, name=name, kind=kind, parent=parent, **kwargs)
        self._stopped = False
        self.device_manager = device_manager
        self._move_thread = None
        self.service_cfg = None
        self.scaninfo = None
        # Init scan parameters
        self.scan_parameter = ScanParameter()

        self.timeout_for_pvwait = 2.5
        self.readback.name = self.name
        # Wait for connection on all components, ensure IOC is connected
        self.wait_for_connection(all_signals=True, timeout=5)

        if device_manager:
            self.device_manager = device_manager
        else:
            self.device_manager = bec_utils.DMMock()

        self.connector = self.device_manager.connector
        self._update_scaninfo()
        self._on_init()

    def _on_init(self):
        """Action to be executed on initialization of the device"""
        self.scan_control.scan_progress.subscribe(self._progress_update, run=False)

    def _progress_update(self, value, **kwargs) -> None:
        """Callback method to update the scan progress, runs a callback
        to SUB_PROGRESS subscribers, i.e. BEC.

        Args:
            value (int) : current progress value
        """
        max_value = 100
        self._run_subs(
            sub_type=self.SUB_PROGRESS,
            value=value,
            max_value=max_value,
            done=bool(max_value == value),
        )

    def _update_scaninfo(self) -> None:
        """Connect to the ScanInfo mixin"""
        self.scaninfo = bec_scaninfo_mixin.BecScaninfoMixin(self.device_manager)
        self.scaninfo.load_scan_metadata()

    @property
    def stopped(self) -> bool:
        """Return the stopped flag. If True, the motion is stopped."""
        return self._stopped

    def stop(self, *, success=False) -> None:
        """Stop any motion on the positioner

        Args:
            success (bool) : Flag to indicate if the motion was successful
        """
        self.move_stop.put(1)
        self._stopped = True
        if self._move_thread is not None:
            self._move_thread.join()
            self._move_thread = None
        super().stop(success=success)

    def stop_scan(self) -> None:
        """Stop the currently running scan gracefully, this finishes the running oscillation."""
        self.scan_control.scan_stop.put(1)

    # -------------- Positioner specific methods -----------------#
    @property
    def limits(self) -> tuple:
        """Return limits of the Bragg positioner"""
        if self.move_type.get() == MoveType.ENERGY:
            return (self.low_lim.get(), self.high_lim.get())
        return (self.low_limit_angle.get(), self.high_limit_angle.get())

    @property
    def low_limit(self) -> float:
        """Return low limit of axis"""
        return self.limits[0]

    @property
    def high_limit(self) -> float:
        """Return high limit of axis"""
        return self.limits[1]

    @property
    def egu(self) -> str:
        """Return the engineering units of the positioner"""
        if self.move_type.get() == MoveType.ENERGY:
            return "eV"
        return "deg"

    @property
    def position(self) -> float:
        """Return the current position of Mo1Bragg, considering the move type"""
        move_type = self.move_type.get()
        move_cpt = self.readback if move_type == MoveType.ENERGY else self.feedback_pos_angle
        return move_cpt.get()

    # pylint: disable=arguments-differ
    def check_value(self, value: float) -> None:
        """Method to check if a value is within limits of the positioner.
        Called by PositionerBase.move()

        Args:
            value (float) : value to move axis to.
        """
        low_limit, high_limit = self.limits

        if low_limit < high_limit and not low_limit <= value <= high_limit:
            raise LimitError(f"position={value} not within limits {self.limits}")

    def _move_and_finish(
        self,
        target_pos: float,
        move_cpt: Cpt,
        read_cpt: Cpt,
        status: DeviceStatus,
        update_frequency: float = 0.1,
    ) -> None:
        """Method to be called in the move thread to move the Bragg positioner
        to the target position.

        Args:
            target_pos (float)      : target position for the motion
            move_cpt (Cpt)          : component to set the target position on the IOC,
                                      either setpoint or setpoint_abs_angle depending
                                      on the move type
            read_cpt (Cpt)          : component to read the current position of the motion,
                                      readback or feedback_pos_angle
            status (DeviceStatus)   : status object to set the status of the motion
            update_frequency (float): Optional, frequency to update the current position of
                                      the motion, defaults to 0.1s
        """
        try:
            # Set the target position on IOC
            move_cpt.put(target_pos)
            self.move_abs.put(1)
            # Currently sleep is needed due to delay in updates on PVs, maybe time can be reduced
            time.sleep(0.5)
            while self.motor_is_moving.get() == 0:
                if self.stopped:
                    raise DeviceStopError(f"Device {self.name} was stopped")
                time.sleep(update_frequency)
            # pylint: disable=protected-access
            status.set_finished()
        # pylint: disable=broad-except
        except Exception as exc:
            content = traceback.format_exc()
            logger.error(f"Error in move thread of device {self.name}: {content}")
            status.set_exception(exc=exc)

    def move(self, value: float, move_type: str | MoveType = None, **kwargs) -> DeviceStatus:
        """Move the Bragg positioner to the specified value, allows to
        switch between move types angle and energy.

        Args:
            value (float) : target value for the motion
            move_type (str | MoveType) : Optional, specify the type of move,
                                         either 'energy' or 'angle'

        Returns:
            DeviceStatus : status object to track the motion
        """
        self._stopped = False
        if move_type is not None:
            self.move_type.put(move_type)
        move_type = self.move_type.get()
        move_cpt = self.setpoint if move_type == MoveType.ENERGY else self.setpoint_abs_angle
        read_cpt = self.readback if move_type == MoveType.ENERGY else self.feedback_pos_angle

        self.check_value(value)
        status = DeviceStatus(device=self)

        self._move_thread = threading.Thread(
            target=self._move_and_finish, args=(value, move_cpt, read_cpt, status, 0.1)
        )
        self._move_thread.start()
        return status

    # -------------- End of Positioner specific methods -----------------#

    # -------------- MO1 Bragg specific methods -----------------#

    def set_xtal(
        self,
        xtal_enum: Literal["111", "311"],
        offset_si111: float = None,
        offset_si311: float = None,
        d_spacing_si111: float = None,
        d_spacing_si311: float = None,
    ) -> None:
        """Method to set the crystal parameters of the Bragg positioner

        Args:
            xtal_enum (Literal["111", "311"]) : Enum to set the crystal orientation
            offset_si111 (float) : Offset for the 111 crystal
            offset_si311 (float) : Offset for the 311 crystal
            d_spacing_si111 (float) : d-spacing for the 111 crystal
            d_spacing_si311 (float) : d-spacing for the 311 crystal
        """
        if offset_si111 is not None:
            self.crystal.offset_si111.put(offset_si111)
        if offset_si311 is not None:
            self.crystal.offset_si311.put(offset_si311)
        if d_spacing_si111 is not None:
            self.crystal.d_spacing_si111.put(d_spacing_si111)
        if d_spacing_si311 is not None:
            self.crystal.d_spacing_si311.put(d_spacing_si311)
        if xtal_enum == "111":
            crystal_set = 0
        elif xtal_enum == "311":
            crystal_set = 1
        else:
            raise ValueError(
                f"Invalid argument for xtal_enum : {xtal_enum}, choose from '111' or '311'"
            )
        self.crystal.xtal_enum.put(crystal_set)
        self.crystal.set_offset.put(1)

    def set_xas_settings(self, low: float, high: float, scan_time: float) -> None:
        """Set XAS parameters for upcoming scan.

        Args:
            low (float): Low energy/angle value of the scan
            high (float): High energy/angle value of the scan
            scan_time (float): Time for a half oscillation
        """
        move_type = self.move_type.get()
        if move_type == MoveType.ENERGY:
            self.scan_settings.s_scan_energy_lo.put(low)
            self.scan_settings.s_scan_energy_hi.put(high)
        else:
            self.scan_settings.s_scan_angle_lo.put(low)
            self.scan_settings.s_scan_angle_hi.put(high)
        self.scan_settings.s_scan_scantime.put(scan_time)

    @typechecked
    def convert_angle_energy(
        self,
        mode:Literal["AngleToEnergy", "EnergyToAngle"],
        inp:float
    ) -> float:
        """Calculate energy to angle or vice versa

        Args:
            mode (Literal["AngleToEnergy", "EnergyToAngle"]): Mode of calculation
            input (float): Either angle or energy
        
        Returns:
            output (float): Converted angle or energy
        """
        self.calculator.calc_reset.put(0)
        self.calculator.calc_reset.put(1)

        if not self.wait_for_signals(
            signal_conditions=[(self.calculator.calc_done.get, 0)],
            timeout=self.timeout_for_pvwait,
            check_stopped=True,
        ):
            raise TimeoutError(
                f"Timeout after {self.timeout_for_pvwait} while waiting for calc done,"
            )
        if mode == "AngleToEnergy":
            self.calculator.calc_angle.put(inp)
        elif mode == "EnergyToAngle":
            self.calculator.calc_energy.put(inp)

        if not self.wait_for_signals(
            signal_conditions=[(self.calculator.calc_done.get, 1)],
            timeout=self.timeout_for_pvwait,
            check_stopped=True,
        ):
            raise TimeoutError(
                f"Timeout after {self.timeout_for_pvwait} while waiting for calc done,"
            )
        time.sleep(0.25) # Needed due to update frequency of softIOC
        if mode == "AngleToEnergy":
            return self.calculator.calc_energy.get()
        elif mode == "EnergyToAngle":
            return self.calculator.calc_angle.get()

    def set_advanced_xas_settings(
        self,
        low: float,
        high:float,
        scan_time: float,
        p_kink: float,
        e_kink: float
    ) -> None:
        """Set Advanced XAS parameters for upcoming scan.

        Args:
            low (float): Low angle value of the scan in deg
            high (float): High angle value of the scan in deg
            scan_time (float): Time for a half oscillation in s
            p_kink (float): Position of kink in %
            e_kink (float): Energy of kink in eV
        """
        # TODO Add fallback solution for automatic testing, otherwise test will fail
        #      because no monochromator will calculate the angle
        #      Unsure how to implement this

        move_type = self.move_type.get()
        if move_type == MoveType.ENERGY:
            e_kink_deg = self.convert_angle_energy(mode="EnergyToAngle", inp=e_kink)
            # Angle and Energy are inverse proportional!
            high_deg = self.convert_angle_energy(mode="EnergyToAngle", inp=low)
            low_deg = self.convert_angle_energy(mode="EnergyToAngle", inp=high)
        else:
            raise Mo1BraggError("MoveType Angle not implemented for advanced scans, use Energy")

        pos, vel, dt = compute_spline(
            low_deg=low_deg,
            high_deg=high_deg,
            p_kink =p_kink,
            e_kink_deg = e_kink_deg,
            scan_time=scan_time,
        )

        self.scan_settings.a_scan_pos.set(pos)
        self.scan_settings.a_scan_vel.set(vel)
        self.scan_settings.a_scan_time.set(dt)

    def set_xrd_settings(
        self,
        enable_low: bool,
        enable_high: bool,
        num_trigger_low: int,
        num_trigger_high: int,
        exp_time_low: int,
        exp_time_high: int,
        cycle_low: int,
        cycle_high: int,
    ) -> None:
        """Set XRD settings for the upcoming scan.

        Args:
            enable_low (bool): Enable XRD for low energy/angle
            enable_high (bool): Enable XRD for high energy/angle
            num_trigger_low (int): Number of triggers for low energy/angle
            num_trigger_high (int): Number of triggers for high energy/angle
            exp_time_low (int): Exposure time for low energy/angle
            exp_time_high (int): Exposure time for high energy/angle
            cycle_low (int): Cycle for low energy/angle
            cycle_high (int): Cycle for high energy/angle
        """
        self.scan_settings.xrd_enable_hi_enum.put(int(enable_high))
        self.scan_settings.xrd_enable_lo_enum.put(int(enable_low))
        self.scan_settings.xrd_n_trigger_hi.put(num_trigger_high)
        self.scan_settings.xrd_n_trigger_lo.put(num_trigger_low)
        self.scan_settings.xrd_time_hi.put(exp_time_high)
        self.scan_settings.xrd_time_lo.put(exp_time_low)
        self.scan_settings.xrd_every_n_hi.put(cycle_high)
        self.scan_settings.xrd_every_n_lo.put(cycle_low)

    def set_scan_control_settings(self, mode: ScanControlMode, scan_duration: float) -> None:
        """Set the scan control settings for the upcoming scan.

        Args:
            mode (ScanControlMode): Mode for the scan, either simple or advanced
            scan_duration (float): Duration of the scan
        """
        val = ScanControlMode(mode).value
        self.scan_control.scan_mode_enum.put(val)
        self.scan_control.scan_duration.put(scan_duration)

    def _update_scan_parameter(self):
        """Get the scaninfo parameters for the scan."""
        for key, value in self.scaninfo.scan_msg.content["info"]["kwargs"].items():
            if hasattr(self.scan_parameter, key):
                setattr(self.scan_parameter, key, value)

    # -------------- End MO1 Bragg specific methods -----------------#

    # -------------- Flyer Interface methods -----------------#

    def kickoff(self):
        """Kickoff the device, called from BEC."""
        scan_duration = self.scan_control.scan_duration.get()
        #TODO implement better logic for infinite scans, at least bring it up with Debye
        start_func = (
            self.scan_control.scan_start_infinite.put
            if scan_duration < 0.1
            else self.scan_control.scan_start_timer.put
        )
        start_func(1)
        status = self.wait_with_status(
            signal_conditions=[(self.scan_control.scan_status.get, ScanControlScanStatus.RUNNING)],
            timeout=self.timeout_for_pvwait,
            check_stopped=True,
        )
        return status

    def stage(self) -> list[object]:
        """
        Stage the device in preparation for a scan.

        Returns:
            list(object): list of objects that were staged
        """
        if self._staged != Staged.no:
            return super().stage()
        self._stopped = False
        self.scaninfo.load_scan_metadata()
        self.on_stage()
        return super().stage()

    def _check_scan_msg(self, target_state: ScanControlLoadMessage) -> None:
        """Check if the scan message is gettting available

        Args:
            target_state (ScanControlLoadMessage): Target state to check for

        Raises:
            TimeoutError: If the scan message is not available after the timeout
        """
        state = self.scan_control.scan_msg.get()
        if state != target_state:
            logger.warning(
                f"Resetting scan validation in stage for state: {ScanControlLoadMessage(state)}, "
                f"retry .get() on scan_control: {ScanControlLoadMessage(self.scan_control.scan_msg.get())} and sleeping 1s"
            )
            self.scan_control.scan_val_reset.put(1)
            # Sleep to ensure the reset is done
            time.sleep(1)

        if not self.wait_for_signals(
            signal_conditions=[(self.scan_control.scan_msg.get, target_state)],
            timeout=self.timeout_for_pvwait,
            check_stopped=True,
        ):
            raise TimeoutError(
                f"Timeout after {self.timeout_for_pvwait} while waiting for scan status,"
                f" current state: {ScanControlScanStatus(self.scan_control.scan_msg.get())}"
            )

    def on_stage(self) -> None:
        """Actions to be executed when the device is staged."""
        if not self.scaninfo.scan_type == "fly":
            return
        self._check_scan_msg(ScanControlLoadMessage.PENDING)

        scan_name = self.scaninfo.scan_msg.content["info"].get("scan_name", "")
        self._update_scan_parameter()
        if scan_name == "xas_simple_scan":
            self.set_xas_settings(
                low=self.scan_parameter.start,
                high=self.scan_parameter.stop,
                scan_time=self.scan_parameter.scan_time,
            )
            self.set_xrd_settings(
                enable_low=False,
                enable_high=False,
                num_trigger_low=0,
                num_trigger_high=0,
                exp_time_low=0,
                exp_time_high=0,
                cycle_low=0,
                cycle_high=0,
            )
            self.set_scan_control_settings(
                mode=ScanControlMode.SIMPLE, scan_duration=self.scan_parameter.scan_duration
            )
        elif scan_name == "xas_simple_scan_with_xrd":
            self.set_xas_settings(
                low=self.scan_parameter.start,
                high=self.scan_parameter.stop,
                scan_time=self.scan_parameter.scan_time,
            )
            self.set_xrd_settings(
                enable_low=self.scan_parameter.xrd_enable_low,
                enable_high=self.scan_parameter.xrd_enable_high,
                num_trigger_low=self.scan_parameter.num_trigger_low,
                num_trigger_high=self.scan_parameter.num_trigger_high,
                exp_time_low=self.scan_parameter.exp_time_low,
                exp_time_high=self.scan_parameter.exp_time_high,
                cycle_low=self.scan_parameter.cycle_low,
                cycle_high=self.scan_parameter.cycle_high,
            )
            self.set_scan_control_settings(
                mode=ScanControlMode.SIMPLE, scan_duration=self.scan_parameter.scan_duration
            )
        elif scan_name == "xas_advanced_scan":
            self.set_advanced_xas_settings(
                low=self.scan_parameter.start,
                high=self.scan_parameter.stop,
                scan_time=self.scan_parameter.scan_time,
                p_kink=self.scan_parameter.p_kink,
                e_kink=self.scan_parameter.e_kink,
            )
            self.set_xrd_settings(
                enable_low=False,
                enable_high=False,
                num_trigger_low=0,
                num_trigger_high=0,
                exp_time_low=0,
                exp_time_high=0,
                cycle_low=0,
                cycle_high=0,
            )
            self.set_scan_control_settings(
                mode=ScanControlMode.ADVANCED, scan_duration=self.scan_parameter.scan_duration
            )
        elif scan_name == "xas_advanced_scan_with_xrd":
            self.set_advanced_xas_settings(
                low=self.scan_parameter.start,
                high=self.scan_parameter.stop,
                scan_time=self.scan_parameter.scan_time,
                p_kink=self.scan_parameter.p_kink,
                e_kink=self.scan_parameter.e_kink,
            )
            self.set_xrd_settings(
                enable_low=self.scan_parameter.xrd_enable_low,
                enable_high=self.scan_parameter.xrd_enable_high,
                num_trigger_low=self.scan_parameter.num_trigger_low,
                num_trigger_high=self.scan_parameter.num_trigger_high,
                exp_time_low=self.scan_parameter.exp_time_low,
                exp_time_high=self.scan_parameter.exp_time_high,
                cycle_low=self.scan_parameter.cycle_low,
                cycle_high=self.scan_parameter.cycle_high,
            )
            self.set_scan_control_settings(
                mode=ScanControlMode.ADVANCED, scan_duration=self.scan_parameter.scan_duration
            )
        else:
            raise Mo1BraggError(
                f"Scan mode {scan_name} not implemented for scan_type={self.scaninfo.scan_type} on  device {self.name}"
            )
        # Load the scan parameters to the controller
        self.scan_control.scan_load.put(1)
        # Wait for params to be checked from controller
        if not self.wait_for_signals(
            signal_conditions=[(self.scan_control.scan_msg.get, ScanControlLoadMessage.SUCCESS)],
            timeout=self.timeout_for_pvwait,
            check_stopped=True,
        ):
            raise TimeoutError(
                f"Scan parameter validation run into timeout after {self.timeout_for_pvwait} with {ScanControlLoadMessage(self.scan_control.scan_msg.get())}"
            )

    def complete(self) -> DeviceStatus:
        """Complete the acquisition.

        The method returns a DeviceStatus object that resolves to set_finished
        or set_exception once the acquisition is completed.
        """
        status = self.on_complete()
        if isinstance(status, DeviceStatus):
            return status
        status = DeviceStatus(self)
        status.set_finished()
        return status

    def on_complete(self) -> DeviceStatus:
        """Specify actions to be performed for the completion of the acquisition."""
        status = self.wait_with_status(
            signal_conditions=[(self.scan_control.scan_done.get, 1)],
            timeout=None,
            check_stopped=True,
        )
        return status

    def unstage(self) -> list[object]:
        """
        Unstage device after a scan. It has to be possible to call this multiple times.

        Returns:
            list(object): list of objects that were unstaged
        """
        self.check_scan_id()
        self._stopped = False
        self.on_unstage()
        return super().unstage()

    def on_unstage(self) -> None:
        """Actions to be executed when the device is unstaged.
        The checks here ensure that the controller resets the Scan_msg to PENDING state."""
        if self.wait_for_signals(
            signal_conditions=[(self.scan_control.scan_msg.get, ScanControlLoadMessage.PENDING)],
            timeout=self.timeout_for_pvwait,
            check_stopped=False,
        ):
            return

        self.scan_control.scan_val_reset.put(1)
        if not self.wait_for_signals(
            signal_conditions=[(self.scan_control.scan_msg.get, ScanControlLoadMessage.PENDING)],
            timeout=self.timeout_for_pvwait,
            check_stopped=False,
        ):
            raise TimeoutError(
                f"Timeout after {self.timeout_for_pvwait} while waiting for scan validation"
            )

    # -------------- End Flyer Interface methods -----------------#

    # -------------- Utility methods -----------------#

    def check_scan_id(self) -> None:
        """Checks if scan_id has changed and set stopped flagged to True if it has."""
        old_scan_id = self.scaninfo.scan_id
        self.scaninfo.load_scan_metadata()
        if self.scaninfo.scan_id != old_scan_id:
            self._stopped = True

    def wait_for_signals(
        self,
        signal_conditions: list[tuple],
        timeout: float | None = None,
        check_stopped: bool = False,
        interval: float = 0.05,
        all_signals: bool = False,
    ) -> bool:
        """Wrapper around a list of conditions that allows waiting for them to become True.
        For EPICs PVs, an example usage is pasted at the bottom.

        Args:
            signal_conditions (list[tuple]): tuple of executable calls for conditions
                                             (get_current_state, condition) to check
            timeout (float): timeout in seconds
            check_stopped (bool): True if stopped flag should be checked.
                                  The function relies on the self.stopped property to be set
            interval (float): interval in seconds
            all_signals (bool): True if all signals should be True,
                                False if any signal should be True

        Returns:
            bool: True if all signals are in the desired state, False if timeout is reached

            >>> Example usage for EPICS PVs:
            >>> self.wait_for_signals(signal_conditions=[(self.acquiring.get, False)], timeout=5, interval=0.05, check_stopped=True, all_signals=True)
        """

        timer = 0
        while True:
            checks = [
                get_current_state() == condition
                for get_current_state, condition in signal_conditions
            ]
            if check_stopped is True and self.stopped is True:
                return False
            if (all_signals and all(checks)) or (not all_signals and any(checks)):
                return True
            if timeout and timer > timeout:
                return False
            time.sleep(interval)
            timer += interval

    def wait_with_status(
        self,
        signal_conditions: list[tuple],
        timeout: float | None = None,
        check_stopped: bool = False,
        interval: float = 0.05,
        all_signals: bool = False,
        exception_on_timeout: Exception = None,
    ) -> DeviceStatus:
        """Wrapper around wait_for_signals to be started in thread and attach a DeviceStatus object.
        This allows BEC to perform actinos in parallel and not be blocked by method
        calls on a device. Typically used for on_trigger, on_complete methods or also the kickoff.

        Args:
            signal_conditions (list[tuple]): tuple of executable calls for conditions
                                             (get_current_state, condition) to check
            timeout (float): timeout in seconds
            check_stopped (bool): True if stopped flag should be checked
            interval (float): interval in seconds
            all_signals (bool): True if all signals should be True,
                                False if any signal should be True
            exception_on_timeout (Exception): Exception to raise on timeout

        Returns:
            DeviceStatus: DeviceStatus object that resolves either to set_finished or set_exception
        """
        if exception_on_timeout is None:
            exception_on_timeout = DeviceTimeoutError(
                f"Timeout error for {self.name} while waiting for signals {signal_conditions}"
            )

        status = DeviceStatus(device=self)

        def wait_for_signals_wrapper(
            status: DeviceStatus,
            signal_conditions: list[tuple],
            timeout: float,
            check_stopped: bool,
            interval: float,
            all_signals: bool,
            exception_on_timeout: Exception = None,
        ):
            """Convenient wrapper around wait_for_signals to set status based on the result.

            Args:
                status (DeviceStatus): DeviceStatus object to be set
                signal_conditions (list[tuple]): tuple of executable calls for
                                                 conditions (get_current_state, condition) to check
                timeout (float): timeout in seconds
                check_stopped (bool): True if stopped flag should be checked
                interval (float): interval in seconds
                all_signals (bool): True if all signals should be True, False if
                                    any signal should be True
                exception_on_timeout (Exception): Exception to raise on timeout
            """
            try:
                result = self.wait_for_signals(
                    signal_conditions, timeout, check_stopped, interval, all_signals
                )
                if result is True:
                    # pylint: disable=protected-access
                    status.set_finished()
                else:
                    if self.stopped:
                        # INFO This will execute a callback to the parent device.stop() method
                        status.set_exception(exc=DeviceStopError(f"{self.name} was stopped"))
                    else:
                        # INFO This will execute a callback to the parent device.stop() method
                        status.set_exception(exc=exception_on_timeout)
            # pylint: disable=broad-except
            except Exception as exc:
                content = traceback.format_exc()
                logger.warning(f"Error in wait_for_signals in {self.name}; Traceback: {content}")
                # INFO This will execute a callback to the parent device.stop() method
                status.set_exception(exc=exc)

        thread = threading.Thread(
            target=wait_for_signals_wrapper,
            args=(
                status,
                signal_conditions,
                timeout,
                check_stopped,
                interval,
                all_signals,
                exception_on_timeout,
            ),
            daemon=True,
        )
        thread.start()
        return status
