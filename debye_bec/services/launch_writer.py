import argparse
import threading

from bec_lib import bec_logger
from bec_lib.redis_connector import RedisConnector
from bec_lib.service_config import ServiceConfig

from debye_bec.services.NIDAQ_writer import NIDAQWriterService

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--config", default="", help="path to the config file")
clargs = parser.parse_args()
config_path = clargs.config

config = ServiceConfig(config_path)
bec_logger.level = bec_logger.LOGLEVEL.INFO
logger = bec_logger.logger

bec_server = NIDAQWriterService(config=config, connector_cls=RedisConnector)
try:
    event = threading.Event()
    # pylint: disable=E1102
    logger.success("Started NIDAQ writer service")
    event.wait()
except KeyboardInterrupt as e:
    # bec_server.connector.raise_error("KeyboardInterrupt")
    bec_server.shutdown()
    event.set()
    raise e
