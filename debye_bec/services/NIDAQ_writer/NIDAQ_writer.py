from __future__ import annotations

import queue
import threading
import time
import traceback

import h5py
import numpy as np
from bec_lib import bec_logger, messages
from bec_lib.bec_service import BECService
from bec_lib.endpoints import MessageEndpoints
from bec_lib.file_utils import FileWriter
from bec_lib.redis_connector import MessageObject, RedisConnector
from bec_lib.service_config import ServiceConfig

logger = bec_logger.logger


class NIDAQWriterService(BECService):
    """
    A service that receives data from the NIDAQ through Redis and writes it continuously to a file.
    """

    reshape_dataset = True
    use_redis_stream = True

    def __init__(self, config: ServiceConfig, connector_cls: RedisConnector) -> None:
        super().__init__(config=config, connector_cls=connector_cls, unique_service=True)
        self.queue = queue.Queue()
        config = self._service_config.service_config.get("file_writer")
        self.writer_mixin = FileWriter(service_config=config)
        self._scan_status_consumer = None
        self._ni_data_consumer = None
        self._ni_data_event = None
        self._ni_writer = None
        self._ni_writer_event = None

        self.scan_number = None
        self.scan_is_running = False
        self.filename = ""

        self.elapsed_time = 0
        self.start_time = 0
        self._start_scan_status_consumer()
        self._start_ni_data_consumer()
        self._start_ni_writer()

    def _start_scan_status_consumer(self) -> None:
        """
        Start the scan consumer.
        """
        self._scan_status_consumer = self.connector.consumer(
            MessageEndpoints.scan_status(), cb=self._scan_status_callback, parent=self
        )
        self._scan_status_consumer.start()

    @staticmethod
    def _scan_status_callback(message: MessageObject, parent: NIDAQWriterService) -> None:
        """
        Callback for scan status messages.
        """
        msg = message.value
        if not msg:
            return
        parent.handle_scan_status(msg)

    def _start_ni_data_consumer(self) -> None:
        """
        Start the NI data consumer.
        """

        self._ni_data_event = threading.Event()
        self._ni_data_consumer = threading.Thread(target=self._run_read_loop, daemon=True)
        self._ni_data_consumer.start()

    def _start_ni_writer(self) -> None:
        """
        Start the NI data writer.
        """
        self._ni_writer_event = threading.Event()
        self._ni_writer = threading.Thread(target=self._write_data, daemon=True)
        self._ni_writer.start()

    def _run_read_loop(self) -> None:
        """
        Run the read loop.
        """
        while not self._ni_data_event.is_set():
            self._read_data()

    def _read_data(self):
        """
        Read data from Redis.
        """
        if not self.scan_is_running:
            time.sleep(0.01)
            return

        self.filename = self.writer_mixin.compile_full_filename(self.scan_number, "ni.h5")

        start_time = time.time()
        if self.use_redis_stream:
            msg = self.connector.xread("ni_data")

            if msg:
                try:
                    num_msgs = len(msg[0][1])
                    logger.debug(
                        f"Received {num_msgs} messages in {time.time() - start_time} seconds"
                    )
                    msgs = [messages.DeviceMessage.loads(m[1][b"device_msg"]) for m in msg[0][1]]
                    start_time = time.time()
                    self.handle_ni_data(msgs)
                    logger.debug(
                        f"Handled {num_msgs} messages in {time.time() - start_time} seconds"
                    )
                except Exception as exc:
                    content = traceback.format_exc()
                    logger.error(f"Failed to parse message: {content}")
            time.sleep(0.01)
        else:
            msgs = self.connector._redis_conn.lpop("ni_data:val", 20)
            time.sleep(0.001)
            if msgs:
                try:
                    msgs = [messages.DeviceMessage.loads(msg) for msg in msgs]
                    print(f"Received {len(msgs)} messages in {time.time() - start_time} seconds")
                    start_time = time.time()
                    self.handle_ni_data(msgs)
                    print(f"Handled {len(msgs)} messages in {time.time() - start_time} seconds")
                except Exception as exc:
                    content = traceback.format_exc()
                    logger.error(f"Failed to parse message: {content}")

    def handle_scan_status(self, msg: messages.ScanStatusMessage) -> None:
        """
        Handle scan status messages.

        Args:
            msg: The scan status message.
        """
        status = msg.content["status"]
        if status == "open":
            self.scan_number = msg.content["info"].get("scan_number")
            if self.scan_number is not None:
                self.scan_is_running = True
        else:
            self.scan_is_running = False

    def handle_ni_data(self, msgs: list[messages.DeviceMessage]) -> None:
        """
        Receive NI data messages and write them to the writer queue.

        Args:
            msgs: The NI data messages.
        """
        logger.info(f"Received {len(msgs)} NI data messages")

        # concatenate all messages
        signals = {}

        for key in msgs[0].content["signals"]:
            signals[key] = np.concatenate([msg.content["signals"][key]["value"] for msg in msgs])

        # write data to queue
        self.queue.put(signals)

    def _write_data(self) -> None:
        """
        Get data from the writer queue and write it to disk.
        """
        while not self._ni_writer_event.is_set():
            signals = self.queue.get()
            logger.info(f"Remaining tasks: {self.queue.qsize()}")
            self.write_data(signals)
            self.queue.task_done()

    def write_data(self, signals: dict) -> None:
        """
        Write data to disk.

        Args:
            signals: The signals to write to disk.
        """
        # create a new file if it doesn't exist, otherwise append to it
        logger.info("Writing NI data to HDF5 file")
        start_time = time.time()
        if not self.filename:
            return
        with h5py.File(self.filename, "a") as file:
            if self.reshape_dataset:
                for key in signals:
                    # if the dataset already exists, append to it
                    if key in file:
                        dataset = file[key]
                        dataset.resize(dataset.shape[0] + len(signals[key]), axis=0)
                        dataset[-len(signals[key]) :] = signals[key]
                    # otherwise create a new dataset
                    else:
                        file.create_dataset(key, data=signals[key], chunks=True, maxshape=(None,))
            else:
                # get all group names
                group_names = list(file.keys())

                # get max dataset number
                dataset_num = [int(name.split("_")[1]) for name in group_names if "dataset" in name]

                if dataset_num:
                    dataset_num = max(dataset_num) + 1
                else:
                    dataset_num = 0
                group = file.create_group(f"dataset_{dataset_num}")
                for key in signals:
                    group.create_dataset(key, data=signals[key], chunks=True, maxshape=(None,))
        logger.info(f"Finished writing NI data in {time.time() - start_time} seconds")
