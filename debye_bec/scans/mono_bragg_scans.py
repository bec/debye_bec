"""This module contains the scan classes for the mono bragg motor of the Debye beamline."""

import time

import numpy as np
from bec_lib.device import DeviceBase
from bec_lib.logger import bec_logger
from bec_server.scan_server.scans import AsyncFlyScanBase

logger = bec_logger.logger


class XASSimpleScan(AsyncFlyScanBase):
    """Class for the XAS simple scan"""

    scan_name = "xas_simple_scan"
    scan_type = "fly"
    scan_report_hint = "device_progress"
    required_kwargs = []
    use_scan_progress_report = False
    pre_move = False
    gui_config = {
        "Movement Parameters": ["start", "stop"],
        "Scan Parameters": ["scan_time", "scan_duration"],
    }

    def __init__(
        self,
        start: float,
        stop: float,
        scan_time: float,
        scan_duration: float,
        motor: DeviceBase = "mo1_bragg",
        **kwargs,
    ):
        """The xas_simple_scan is used to start a simple oscillating scan on the mono bragg motor.
        Start and Stop define the energy range for the scan, scan_time is the time for one scan
        cycle and scan_duration is the duration of the scan. If scan duration is set to 0, the
        scan will run infinitely.

        Args:
            start (float): Start energy for the scan.
            stop (float): Stop energy for the scan.
            scan_time (float): Time for one scan cycle.
            scan_duration (float): Duration of the scan.
            motor (DeviceBase, optional): Motor device to be used for the scan.
                                          Defaults to "mo1_bragg".
        Examples:
            >>> scans.xas_simple_scan(start=8000, stop=9000, scan_time=1, scan_duration=10)
        """
        super().__init__(**kwargs)
        self.motor = motor
        self.start = start
        self.stop = stop
        self.scan_time = scan_time
        self.scan_duration = scan_duration
        self.primary_readout_cycle = 1

    def prepare_positions(self):
        """Prepare the positions for the scan.

        Use here only start and end energy defining the range for the scan.
        """
        self.positions = np.array([self.start, self.stop], dtype=float)
        self.num_pos = None
        yield None

    def pre_scan(self):
        """Pre Scan action. Ensure the motor movetype is set to energy, then check
        limits for start/end energy.
        #TODO Remove once the motor movetype is removed and ANGLE motion is a pseudo motor.
        """
        yield from self.stubs.send_rpc_and_wait(self.motor, "move_type.set", "energy")

        self._check_limits()
        # Ensure parent class pre_scan actions to be called.
        yield from super().pre_scan()

    def scan_report_instructions(self):
        """
        Return the instructions for the scan report.
        """
        yield from self.stubs.scan_report_instruction({"device_progress": [self.motor]})

    def scan_core(self):
        """Run the scan core.
        Kickoff the oscillation on the Bragg motor and wait for the completion of the motion.
        """
        # Start the oscillation on the Bragg motor.
        yield from self.stubs.kickoff(device=self.motor)
        complete_status = yield from self.stubs.complete(device=self.motor, wait=False)

        while not complete_status.done:
            # Readout monitored devices
            yield from self.stubs.read(group="monitored", point_id=self.point_id)
            time.sleep(self.primary_readout_cycle)
            self.point_id += 1

        self.num_pos = self.point_id + 1


class XASSimpleScanWithXRD(XASSimpleScan):
    """Class for the XAS simple scan with XRD"""

    scan_name = "xas_simple_scan_with_xrd"
    gui_config = {
        "Movement Parameters": ["start", "stop"],
        "Scan Parameters": ["scan_time", "scan_duration"],
        "Low Energy Range": ["xrd_enable_low", "num_trigger_low", "exp_time_low", "cycle_low"],
        "High Energy Range": ["xrd_enable_high", "num_trigger_high", "exp_time_high", "cycle_high"],
    }

    def __init__(
        self,
        start: float,
        stop: float,
        scan_time: float,
        scan_duration: float,
        xrd_enable_low: bool,
        num_trigger_low: int,
        exp_time_low: float,
        cycle_low: int,
        xrd_enable_high: bool,
        num_trigger_high: int,
        exp_time_high: float,
        cycle_high: float,
        motor: DeviceBase = "mo1_bragg",
        **kwargs,
    ):
        """The xas_simple_scan_with_xrd is an oscillation motion on the mono motor
        with XRD triggering at low and high energy ranges.
        If scan duration is set to 0, the scan will run infinitely.

        Args:
            start (float): Start energy for the scan.
            stop (float): Stop energy for the scan.
            scan_time (float): Time for one oscillation .
            scan_duration (float): Total duration of the scan.
            xrd_enable_low (bool): Enable XRD triggering for the low energy range.
            num_trigger_low (int): Number of triggers for the low energy range.
            exp_time_low (float): Exposure time for the low energy range.
            cycle_low (int): Specify how often the triggers should be considered,
                             every nth cycle for low
            xrd_enable_high (bool): Enable XRD triggering for the high energy range.
            num_trigger_high (int): Number of triggers for the high energy range.
            exp_time_high (float): Exposure time for the high energy range.
            cycle_high (int): Specify how often the triggers should be considered,
                              every nth cycle for high
            motor (DeviceBase, optional): Motor device to be used for the scan.
                                          Defaults to "mo1_bragg".

        Examples:
            >>> scans.xas_simple_scan_with_xrd(start=8000, stop=9000, scan_time=1, scan_duration=10, xrd_enable_low=True, num_trigger_low=5, cycle_low=2, exp_time_low=100, xrd_enable_high=False, num_trigger_high=3, cycle_high=1, exp_time_high=1000)
        """
        super().__init__(
            start=start,
            stop=stop,
            scan_time=scan_time,
            scan_duration=scan_duration,
            motor=motor,
            **kwargs,
        )
        self.xrd_enable_low = xrd_enable_low
        self.num_trigger_low = num_trigger_low
        self.exp_time_low = exp_time_low
        self.cycle_low = cycle_low
        self.xrd_enable_high = xrd_enable_high
        self.num_trigger_high = num_trigger_high
        self.exp_time_high = exp_time_high
        self.cycle_high = cycle_high


class XASAdvancedScan(XASSimpleScan):
    """Class for the XAS advanced scan"""

    scan_name = "xas_advanced_scan"
    gui_config = {
        "Movement Parameters": ["start", "stop"],
        "Scan Parameters": ["scan_time", "scan_duration"],
        "Spline Parameters": ["p_kink", "e_kink"],
    }

    def __init__(
        self,
        start: float,
        stop: float,
        scan_time: float,
        scan_duration: float,
        p_kink: float,
        e_kink: float,
        motor: DeviceBase = "mo1_bragg",
        **kwargs,
    ):
        """The xas_advanced_scan is an oscillation motion on the mono motor.
        Start and Stop define the energy range for the scan, scan_time is the
        time for one scan cycle and scan_duration is the duration of the scan.
        If scan duration is set to 0, the scan will run infinitely.
        p_kink and e_kink add a kink to the motion profile to slow down in the
        exafs region of the scan.

        Args:
            start (float): Start angle for the scan.
            stop (float): Stop angle for the scan.
            scan_time (float): Time for one oscillation .
            scan_duration (float): Total duration of the scan.
            p_kink (float): Position of the kink.
            e_kink (float): Energy of the kink.
            motor (DeviceBase, optional): Motor device to be used for the scan.
                                          Defaults to "mo1_bragg".

        Examples:
            >>> scans.xas_advanced_scan(start=10000, stop=12000, scan_time=0.5, scan_duration=10, p_kink=50, e_kink=10500)
        """
        super().__init__(
            start=start,
            stop=stop,
            scan_time=scan_time,
            scan_duration=scan_duration,
            motor=motor,
            **kwargs,
        )
        self.p_kink = p_kink
        self.e_kink = e_kink


class XASAdvancedScanWithXRD(XASAdvancedScan):
    """Class for the XAS advanced scan with XRD"""

    scan_name = "xas_advanced_scan_with_xrd"
    gui_config = {
        "Movement Parameters": ["start", "stop"],
        "Scan Parameters": ["scan_time", "scan_duration"],
        "Spline Parameters": ["p_kink", "e_kink"],
        "Low Energy Range": ["xrd_enable_low", "num_trigger_low", "exp_time_low", "cycle_low"],
        "High Energy Range": ["xrd_enable_high", "num_trigger_high", "exp_time_high", "cycle_high"],
    }

    def __init__(
        self,
        start: float,
        stop: float,
        scan_time: float,
        scan_duration: float,
        p_kink: float,
        e_kink: float,
        xrd_enable_low: bool,
        num_trigger_low: int,
        exp_time_low: float,
        cycle_low: int,
        xrd_enable_high: bool,
        num_trigger_high: int,
        exp_time_high: float,
        cycle_high: float,
        motor: DeviceBase = "mo1_bragg",
        **kwargs,
    ):
        """The xas_advanced_scan is an oscillation motion on the mono motor
        with XRD triggering at low and high energy ranges.
        Start and Stop define the energy range for the scan, scan_time is the time for
        one scan cycle and scan_duration is the duration of the scan. If scan duration
        is set to 0, the scan will run infinitely. p_kink and e_kink add a kink to the
        motion profile to slow down in the exafs region of the scan.

        Args:
            start (float): Start angle for the scan.
            stop (float): Stop angle for the scan.
            scan_time (float): Time for one oscillation .
            scan_duration (float): Total duration of the scan.
            p_kink (float): Position of kink.
            e_kink (float): Energy of the kink.
            xrd_enable_low (bool): Enable XRD triggering for the low energy range.
            num_trigger_low (int): Number of triggers for the low energy range.
            exp_time_low (float): Exposure time for the low energy range.
            cycle_low (int): Specify how often the triggers should be considered,
                             every nth cycle for low
            xrd_enable_high (bool): Enable XRD triggering for the high energy range.
            num_trigger_high (int): Number of triggers for the high energy range.
            exp_time_high (float): Exposure time for the high energy range.
            cycle_high (int): Specify how often the triggers should be considered,
                              every nth cycle for high
            motor (DeviceBase, optional): Motor device to be used for the scan.
                                          Defaults to "mo1_bragg".

        Examples:
            >>> scans.xas_advanced_scan_with_xrd(start=10000, stop=12000, scan_time=0.5, scan_duration=10, p_kink=50, e_kink=10500, xrd_enable_low=True, num_trigger_low=5, cycle_low=2, exp_time_low=100, xrd_enable_high=False, num_trigger_high=3, cycle_high=1, exp_time_high=1000)
        """
        super().__init__(
            start=start,
            stop=stop,
            scan_time=scan_time,
            scan_duration=scan_duration,
            p_kink=p_kink,
            e_kink=e_kink,
            motor=motor,
            **kwargs,
        )
        self.p_kink = p_kink
        self.e_kink = e_kink
        self.xrd_enable_low = xrd_enable_low
        self.num_trigger_low = num_trigger_low
        self.exp_time_low = exp_time_low
        self.cycle_low = cycle_low
        self.xrd_enable_high = xrd_enable_high
        self.num_trigger_high = num_trigger_high
        self.exp_time_high = exp_time_high
        self.cycle_high = cycle_high
