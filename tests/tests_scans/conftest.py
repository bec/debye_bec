# pylint: skip-file
from functools import partial

import pytest
from bec_server.device_server.tests.utils import DeviceMockType, DMMock
from bec_server.scan_server.tests.fixtures import (
    ScanStubStatusMock,
    connector_mock,
    instruction_handler_mock,
)


@pytest.fixture
def device_manager_mock():
    device_manager = DMMock()
    device_manager.add_device(
        "mo1_bragg", dev_type=DeviceMockType.POSITIONER, readout_priority="monitored"
    )
    device_manager.add_device("samx")
    device_manager.add_device(
        "eiger", dev_type=DeviceMockType.SIGNAL, readout_priority="monitored", software_trigger=True
    )
    device_manager.add_device("bpm4i", dev_type=DeviceMockType.SIGNAL, readout_priority="monitored")
    yield device_manager


@pytest.fixture
def scan_assembler(instruction_handler_mock, device_manager_mock):
    def _assemble_scan(scan_class, *args, **kwargs):
        return scan_class(*args, **kwargs)

    return partial(
        _assemble_scan,
        instruction_handler=instruction_handler_mock,
        device_manager=device_manager_mock,
    )
