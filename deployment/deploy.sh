# deployment script to be translated to Ansible

# can be removed once we have the autodeployment in place
BEAMLINE_REPO=gitlab.psi.ch:bec/debye-bec.git
git clone git@$BEAMLINE_REPO

module add psi-python311/2024.02

# start redis
docker run --network=host --name redis-bec -d redis
# alternative:
# conda install -y redis; redis-server &


# get the target versions for ophyd_devices and BEC
source ./debye-bec/deployment/autodeploy_versions

git clone -b $OPHYD_DEVICES_AUTODEPLOY_VERSION https://gitlab.psi.ch/bec/ophyd_devices.git
git clone -b $BEC_AUTODEPLOY_VERSION https://gitlab.psi.ch/bec/bec.git

# install BEC
cd bec
source ./bin/install_bec_dev.sh

cd ../
# start the BEC server
bec-server start --config ./debye-bec/deployment/bec-server-config.yaml
